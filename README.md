**Azure account is forbidden now for Russia (validating debit card is impossible, tried many times with many cards), 
therefore I ran notebook locally with `mlflow server`.**

**I converted `.dbc`-notebook to `.ipynb` in order to be able to do task locally**

_(dbc-notebooks are suitable only for Databricks, which is forbidden now for me)_.



**Notebook-file is `spark-ml.ipynb`, it's the main file in the project.**
**It contains all the results for each executed `cell` of code.**


I've added `mlruns` directory (with **runs' artifacts**) to `.gitignore`, 

because it's too **large** (but I've attached screenshot of this directory, it's below).


Screenshots with comments are behind (follow) instruction-for-use.

**Instruction for use:**


Install virtual env in `venv/` directory

Fill in `venv/bin/activate` you own env-variables' values from `.env.example`

Activate virtual env and your env-variables by `source venv/bin/activate`

Install requirements via `pip install -r requirements.txt`

To make the script (which runs mlflow server) executable:

`sudo chmod +x ./scripts/mlflow_run_server.sh`

To run mlflow local server:

`./scripts/mlflow_run_server.sh`

To run Model-serving (only after saving it in Model Registry):

`sudo chmod +x ./scripts/mlflow_serve_model.sh`

`./scripts/mlflow_serve_model.sh`

All code is written in `spark-ml.ipynb`.




**Screenshots with comments.**

Starting mlflow local server:

![](screenshots/1.start_mlflow_server.png)

Random forest local run in UI (`0.0.0.0:5000`):

![](screenshots/2.random_forest_run.png)

Model's 1-st version (random forest) saved in Model Registry:

![](screenshots/3.saved_model_v1.png)

Changing stage of 1-st version to "_production_":

![](screenshots/4.model_v1_production.png)

Running xgboost's hyperparameter sweep with spark-trials:

![](screenshots/5.running_xgboost.png)

Listed xgboost runs:

![](screenshots/6.listed_xgboost_runs.png)

Saved artifacts for each xgboost-run:

![](screenshots/7.saved_xgboost_artifacts.png)

Registering 2-nd version for our model (best xgboost run):

![](screenshots/8.saved_model_v2_by_xgboost.png)

Archiving 1-st version of our model and changing stage of 2-nd to _"production"_:

![](screenshots/9.model_v2_production.png)

Starting Model-serving (endpoint for model):

![](screenshots/10.start_model_serving.png)


Comparing predictions of model and served model. They are same:

![](screenshots/11.comparing_predictions.png)






